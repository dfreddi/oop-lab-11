namespace baseCs
{
    public interface IRobot
    {
        /**
         * Moves the robot up by one unit
         * 
         * @return true if a movement has been performed
         */
        bool MoveUp();
        
        /**
         * Moves the robot down by one unit
         * 
         * @return true if a movement has been performed
         */
        bool MoveDown();

        /**
         * Moves the robot left by one unit
         * 
         * @return true if a movement has been performed
         */
        bool MoveLeft();

        /**
         * Moves the robot right by one unit
         * 
         * @return true if a movement has been performed
         */
        bool MoveRight();
        
        /**
         * Fully recharge the robot
         */
        void Recharge();
        
        double BatteryLevel { get; }
        
        IPosition2D Position { get; }
    }
}