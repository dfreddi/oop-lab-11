using System;

namespace baseCs
{
    public class BaseRobot : IRobot
    {
        
        public static readonly double BATTERY_FULL = 100;
        public static readonly double MOVEMENT_DELTA_CONSUMPTION = 1.2;
        private static readonly int MOVEMENT_DELTA = 1;

        private double _batteryLevel;
        private readonly RobotEnvironment _environment;
        private readonly string _robotName;

        
        public BaseRobot(string robotName)
        {
            _environment = new RobotEnvironment(new RobotPosition(0, 0));
            _robotName = robotName;
            _batteryLevel = BATTERY_FULL;
        }
        
        protected void ConsumeBattery(double amount)
        {
            if (_batteryLevel >= amount)
            {
                _batteryLevel -= amount;
            }
            else
            {
                _batteryLevel = 0;
            }
        }
        
        private void ConsumeBatteryForMovement()
        {
            ConsumeBattery(BatteryRequirementForMovement);
        }

        
        protected double BatteryRequirementForMovement => MOVEMENT_DELTA_CONSUMPTION;

        
        public double BatteryLevel => Math.Round(_batteryLevel * 100d) / BATTERY_FULL;


        public IPosition2D Position => _environment.Position;

        
        protected bool IsBatteryEnough(double operationCost)
        {
            return _batteryLevel > operationCost;
        }

        
        protected void Log(string msg)
        {
            Console.WriteLine($"[{_robotName}]: {msg}");
        }
        
        private bool Move(int dx, int dy) 
        {
            if (IsBatteryEnough(BatteryRequirementForMovement))
            {
                if (_environment.Move(dx, dy))
                {
                    ConsumeBatteryForMovement();
                    Log($"Moved to position{_environment.Position}. Battery: {BatteryLevel}%.");
                    return true;
                }
                Log($"Can not move of ({dx},{dy}) the robot is touching the world boundary: current position is {_environment.Position}");
            }
            else
            {
                Log($"Can not move, not enough battery. Required: {BatteryRequirementForMovement}, available: {_batteryLevel} ({BatteryLevel}%)");
            }
            return false;
        }
        
        

        public bool MoveDown()
        {
            return Move(0, -MOVEMENT_DELTA);
        }

        public bool MoveLeft()
        {
            return Move(-MOVEMENT_DELTA, 0);
        }

        public bool MoveRight()
        {
            return Move(MOVEMENT_DELTA, 0);
        }

        public bool MoveUp()
        {
            return Move(0, MOVEMENT_DELTA);
        }
        
        public void Recharge()
        {
            _batteryLevel = BATTERY_FULL;
        }

        public override string ToString()
        {
            return _robotName;
        }
    }
}