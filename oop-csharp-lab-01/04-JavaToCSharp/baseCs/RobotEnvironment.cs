namespace baseCs
{
    public class RobotEnvironment
    {
        
        public static readonly int X_UPPER_LIMIT = 50;
        
        public static readonly int X_LOWER_LIMIT = 0;
        
        public static readonly int Y_UPPER_LIMIT = 80;
        
        public static readonly int Y_LOWER_LIMIT = 0;

        private IPosition2D _position;
        
        public RobotEnvironment( RobotPosition position )
        {
            _position = position;
        }

        public IPosition2D Position => _position;

        protected bool IsWithinWorld(IPosition2D p) 
        {
            var x = p.X;
            var y = p.Y;
            return x >= X_LOWER_LIMIT && x <= X_UPPER_LIMIT && y >= Y_LOWER_LIMIT && y <= Y_UPPER_LIMIT;
        }
        
        public bool Move(int dx, int dy)
        {
            var newPos = _position.SumVector(dx, dy);
            if (!IsWithinWorld(newPos)) 
                return false;
            _position = newPos;
            return true;
        }
    }
}