namespace baseCs
{
    public class RobotPosition : IPosition2D
    {
        private readonly int _x;
        private readonly int _y;
        
        public RobotPosition(int x,  int y) 
        {
            _x = x;
            _y = y;
        }

        public int X => this._x;

        public int Y => this._y;


        public override bool Equals(object o)
        {
            if (o is IPosition2D) {
                var p = (IPosition2D) o;
                return this._x == p.X && this._y == p.Y;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return _x ^ _y;
        }
        
        public IPosition2D SumVector(int x, int y) 
        {
            return new RobotPosition(_x + x, _y + y);
        }
        
        public IPosition2D SumVector(IPosition2D p) 
        {
            return new RobotPosition(_x + p.X, _y + p.Y);
        }

        public override string ToString()
        {
            return $"[{_x}, {_y}]";
        }
    }
}