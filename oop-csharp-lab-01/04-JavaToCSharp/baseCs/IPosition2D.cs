namespace baseCs
{
    public interface IPosition2D
    {
        int X { get; }

        int Y { get; }
        
        /**
         * @param p
         *            delta movement to sum
         * @return the new position
         */
        IPosition2D SumVector(IPosition2D p);

        /**
         * @param x
         *            X delta
         * @param y
         *            Y delta
         * @return the new position
         */
        IPosition2D SumVector(int x, int y);
    }
}