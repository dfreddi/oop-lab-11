﻿using baseCs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace armsCs
{
    class RobotWithTwoArms : BaseRobot, IRobotWithArms
    {
        public static readonly double TRANSPORT_OBJECT_CONSUMPTION = 0.1;

        private readonly BasicArm leftArm;
        private readonly BasicArm rightArm;

    /**
     * 
     * @param robotName
     * @param batteryLevel
     */
        public RobotWithTwoArms(String robotName) : base(robotName)
        {
            leftArm = new BasicArm("Left arm");
            rightArm = new BasicArm("Right arm");
        }

        protected override double BatteryRequirementForMovement
        {
            get { return base.BatteryRequirementForMovement + CarriedItemsCount * TRANSPORT_OBJECT_CONSUMPTION; }
        }

        private void DoPick(BasicArm arm)
        {
            if (IsBatteryEnough(arm.ConsuptionForPickUp) && !arm.IsGrabbing)
            {
                Log(arm + " is picking an object");
                arm.PickUp();
                ConsumeBattery(arm.ConsuptionForPickUp);
            }
            else
            {
                Log($"Can not grab (battery={this.BatteryLevel},{arm} isGrabbing={arm.IsGrabbing})");
            }
        }

        private void doRelease(BasicArm arm)
        {
            if (isBatteryEnough(arm.getConsuptionForDropDown()) && arm.isGrabbing())
            {
                this.log(arm + " is releasing an object");
                arm.dropDown();
                this.consumeBattery(arm.getConsuptionForDropDown());
            }
            else
            {
                log("Can not release (batteryLevel=" + this.getBatteryLevel() + "," + arm + " isGrabbing="
                        + arm.isGrabbing() + ")");
            }
        }

        public boolean dropDown()
        {
            if (leftArm.isGrabbing())
            {
                doRelease(leftArm);
                return true;
            }
            if (rightArm.isGrabbing())
            {
                doRelease(rightArm);
                return true;
            }
            return false;
        }

        public int getCarriedItemsCount()
        {
            return (leftArm.isGrabbing() ? 1 : 0) + (rightArm.isGrabbing() ? 1 : 0);
        }

        public boolean pickUp()
        {
            if (rightArm.isGrabbing())
            {
                if (leftArm.isGrabbing())
                {
                    return false;
                }
                doPick(leftArm);
            }
            else
            {
                doPick(rightArm);
            }
            return true;
        }
    }
}
