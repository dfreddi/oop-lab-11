﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace armsCs
{
    class BasicArm
    {
        private static readonly double ENERGY_REQUIRED_TO_MOVE = 0.2;
        private static readonly double ENERGY_REQUIRED_TO_GRAB = 0.1;
        private bool grabbing;
        private readonly String _name;

        public BasicArm(String name)
        {
            _name = name;
        }

        public bool IsGrabbing
        {
            get { return grabbing; }
        }

        public void PickUp()
        {
            grabbing = true;
        }

        public void DropDown()
        {
            grabbing = false;
        }

        public double ConsuptionForPickUp
        {
            get { return ENERGY_REQUIRED_TO_MOVE + ENERGY_REQUIRED_TO_GRAB; }
        }

        public double ConsuptionForDropDown
        {
            get { return ENERGY_REQUIRED_TO_MOVE; }
        }

        public override String ToString()
        {
            return _name;
        }
    }
}
