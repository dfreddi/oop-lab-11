﻿using baseCs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace armsCs
{
    interface IRobotWithArms:IRobot
    {
        bool PickUp();

        bool DropDown();

        int CarriedItemsCount { get; }
    }
}
