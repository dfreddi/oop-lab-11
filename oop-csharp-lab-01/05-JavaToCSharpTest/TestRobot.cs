﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using baseCs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _05_JavaToCSharpTest
{
    [TestClass]
    public class TestRobot
    {
        [TestMethod]
        public void Test02()
        {
            int stepsDefault = (int)(BaseRobot.BATTERY_FULL / BaseRobot.MOVEMENT_DELTA_CONSUMPTION);
            BaseRobot r0 = new BaseRobot("R2D2");
            Assert.AreEqual(r0.Position, new RobotPosition(0, 0));
            Assert.AreEqual(r0.BatteryLevel, BaseRobot.BATTERY_FULL);
            var steps = stepsDefault;
            while (r0.MoveUp())
            {
                steps--;
            }
            if (steps == 0)
            {
                Assert.AreEqual(r0.BatteryLevel < BaseRobot.MOVEMENT_DELTA_CONSUMPTION, true);
                r0.Recharge();
                Assert.AreEqual(r0.BatteryLevel, BaseRobot.BATTERY_FULL);
                steps = stepsDefault;
            }
            else
            {
                Assert.AreEqual(r0.Position, new RobotPosition(0, RobotEnvironment.Y_UPPER_LIMIT));
            }
            while (r0.MoveRight())
            {
                steps--;
            }
            if (steps == 0)
            {
                Assert.AreEqual(r0.BatteryLevel < BaseRobot.MOVEMENT_DELTA_CONSUMPTION, true);
                r0.Recharge();
                Assert.AreEqual(r0.BatteryLevel, BaseRobot.BATTERY_FULL);
                steps = stepsDefault;
            }
            else
            {
                Assert.AreEqual(r0.Position, new RobotPosition(RobotEnvironment.X_UPPER_LIMIT, RobotEnvironment.Y_UPPER_LIMIT));
            }
            while (r0.MoveRight())
            {
                steps--;
            }
            if (steps == 0)
            {
                Assert.AreEqual(r0.BatteryLevel < BaseRobot.MOVEMENT_DELTA_CONSUMPTION, true);
                r0.Recharge();
                Assert.AreEqual(r0.BatteryLevel, BaseRobot.BATTERY_FULL);
                steps = stepsDefault;
            }
            else
            {
                Assert.AreEqual(r0.Position, new RobotPosition(RobotEnvironment.X_UPPER_LIMIT, RobotEnvironment.Y_UPPER_LIMIT));
            }
        }
    }
}
