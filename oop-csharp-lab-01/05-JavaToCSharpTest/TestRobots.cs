﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using baseCs;

namespace JavaToCSharpTest
{
    [TestClass]
    static class TestRobots
    {
        [TestMethod]
        public static void TestBaseCs()
        {
            int stepsDefault = (int)(BaseRobot.BatteryFull / BaseRobot.MovementDeltaConsumption);
            BaseRobot r0 = new BaseRobot("R2D2");
            Assert.AreEqual(r0.Position, new RobotPosition(0, 0));
            Assert.AreEqual(r0.BatteryLevel, BaseRobot.BatteryFull);
            int steps = stepsDefault;
            while (r0.MoveUp())
            {
                steps--;
            }
            if (steps == 0)
            {
                Assert.AreEqual(r0.BatteryLevel < BaseRobot.MovementDeltaConsumption, true);
                r0.Recharge();
                Assert.AreEqual(r0.BatteryLevel, BaseRobot.BatteryFull);
                steps = stepsDefault;
            }
            else
            {
                Assert.AreEqual(r0.Position, new RobotPosition(0, RobotEnvironment.YUpperLimit));
            }
            while (r0.MoveRight())
            {
                steps--;
            }
            if (steps == 0)
            {
                Assert.AreEqual(r0.BatteryLevel < BaseRobot.MovementDeltaConsumption, true);
                r0.Recharge();
                Assert.AreEqual(r0.BatteryLevel, BaseRobot.BatteryFull);
                steps = stepsDefault;
            }
            else
            {
                Assert.AreEqual(r0.Position, new RobotPosition(RobotEnvironment.XUpperLimit, RobotEnvironment.YUpperLimit));
            }
            while (r0.MoveRight())
            {
                steps--;
            }
            if (steps == 0)
            {
                Assert.AreEqual(r0.BatteryLevel < BaseRobot.MovementDeltaConsumption, true);
                r0.Recharge();
                Assert.AreEqual(r0.BatteryLevel, BaseRobot.BatteryFull);
                steps = stepsDefault;
            }
            else
            {
                Assert.AreEqual(r0.Position, new RobotPosition(RobotEnvironment.XUpperLimit, RobotEnvironment.YUpperLimit));
            }
        }
    }
}
