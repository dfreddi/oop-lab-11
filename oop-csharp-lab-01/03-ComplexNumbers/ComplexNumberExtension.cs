using System;

namespace ComplexNumbers
{
    static class ComplexNumberExtension
    {
        public static ComplexNum Invert(this ComplexNum c)
        {
            return new ComplexNum(
                c.Re / (Math.Pow(c.Re, 2) + Math.Pow(c.Im, 2)), 
                -c.Im / (Math.Pow(c.Re, 2) + Math.Pow(c.Im, 2))
                );
        }
    }
}