﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        private double re;
        private double im;
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re
        {
            get { return this.re; }
            private set { this.re = value; }
        }

        public double Im
        {
            get { return this.im; }
            private set { this.im = value; }
        }

        // Restituisce il modulo del numero complesso
        public double Module
        {
            get { return Math.Sqrt(Math.Pow(im, 2) + Math.Pow(re, 2)); }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(this.re, -this.im);
            }
        }

        public static ComplexNum operator +(ComplexNum op1, ComplexNum op2)
        {
            return new ComplexNum(op1.Re + op2.Re, op1.Im + op2.Im);
        }

        public static ComplexNum operator -(ComplexNum op1, ComplexNum op2)
        {
            return new ComplexNum(op1.Re - op2.Re, op1.Im - op2.Im);
        }

        public static ComplexNum operator *(ComplexNum op1, ComplexNum op2)
        {
            return new ComplexNum(op1.Re*op2.Re + op1.Im*op2.Im, op1.Re*op2.Im + op2.Re*op1.Im);
        }

        public static ComplexNum operator /(ComplexNum op1, ComplexNum op2)
        {
            return new ComplexNum(
                (op1.Re*op2.Re + op1.Im*op2.Im) / (Math.Pow(op2.Re, 2) + Math.Pow(op2.Im, 2)), 
                (op1.Re*op2.Im + op1.Im*op2.Re) / (Math.Pow(op2.Re, 2) + Math.Pow(op2.Im, 2))
                );
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return $"{Re}+{im}i";
        }
    }
}
