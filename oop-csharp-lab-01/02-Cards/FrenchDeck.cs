using System;

namespace Cards
{
    class FrenchDeck
    {
        private Card[] cards;

        public FrenchDeck()
        {
            cards = new Card[Enum.GetNames(typeof(FrenchValue)).Length * Enum.GetNames(typeof(FrenchSeed)).Length + 2];
        }

        public Card this[FrenchValue value, FrenchSeed seed]
        {
            get
            {
                foreach (Card c in cards)
                {
                    if ((c.Seed == seed.ToString() && c.Value == value.ToString()) || (value.ToString() == "JOLLY" && c.ToString() == "JOLLY"))
                    {
                        return c;
                    }
                }
                return null;
            }
        }
        
        

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            int i = 0;
            foreach (FrenchSeed seed in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
            {
                foreach (FrenchValue value in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                {
                    cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }
            cards[i] = new Card("JOLLY", "");
            cards[i + 1] = new Card("JOLLY", "");
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */

            foreach (Card c in cards)
            {
                Console.WriteLine(c);
            }
        }

    }
    
    enum FrenchSeed
    {
        CUORI,
        QUADRI,
        PICCHE,
        FIORI
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        JACK,
        DONNA,
        RE
    }
}
